/*jshint moz: true, esnext: true */

const data = {
    lebenslauf: [{
        titel: "Beruflich",
        icon: "building",
        abschnitte: [{
            start: "08.2017",
            firma: "Lazard Asset Management",
            position: "IT Program Analyst",
            beschreibung: "Datawarehousing und Business Intelligence.",
            liste: [
                "ETL-Tool-Entwicklung",
                "App-Entwicklung",
                "Jira- und Confluence-Admin",
                "Report-Erstellung"
            ],
            tools: [
                "Pentaho Kettle",
                "Cordova",
                "Confluence",
                "Jira",
                "NodeJS",
                "Jenkins",
                "Eclipse",
                "Visual Studio Code",
                "Excel",
                "Oracle Database Server",
                "Microsoft SQL Server",
                "WinSCP",
                "PuTTY",
                "DBeaver",
                "Sourcetree",
                "Git",
                "SVN"
                ],
            programmiersprachen: [
                {
                    sprache:"Java",
                    frameworks: [
                        "ActiveJDBC",
                        "NanoHTTPD",
                        "Slf4j"
                        ]
                }, {
                    sprache:"JavaScript",
                    frameworks: [
                        "Framework7",
                        "codemirror",
                        "driver.js",
                        "FileSaver.js",
                        "html2canvas",
                        "plot.ly",
                        "PubSub.js",
                        "load-image.js"
                        ]
                },
                {
                    sprache: "Bash"
                },
                {
                    sprache: "SQL"
                }
            ]
        }, {
            ende: "07.2017",
            start: "01.2017",
            firma: "Dion Global Solutions",
            position: "Quality Assurance Engineer",
            beschreibung: "Qualitätssicherung des Workflow und Messaging Systems.",
            liste: [
                "Testautomatisierung mit Tcl und Shell-Skripten",
                "Anforderungsanalyse und Testerstellung"
            ],
            tools: [
                "MySQL Workbench",
                "MS SQL Server Management Studio",
                "VirtualBox",
                "TestLink",
                "Jira",
                "WinSCP",
                "PuTTY",
                "Eclipse",
                "Altova XML Spy"
                ],
            programmiersprachen: [
                {
                    sprache:"Java"
                }, {
                    sprache:"JavaScript",
                    frameworks: [
                        "ExtJS",
                        "codemirror"
                        ]
                },
                {
                    sprache: "PHP"
                },
                {
                    sprache: "SQL"
                },
                {
                    sprache: "TCL"
                }
            ]
        }, {
            start: "07.2016",
            ende: "12.2016",
            position: "Freiberufliche Appentwicklung",
            beschreibung: "Auftrags- und Eigenentwicklung von Desktopsoftware und Apps mit Cordova für Android/Windows Phone.",
            liste: [
                "Entwicklung im Back- und Frontend in Java SE, Javascript und HTML",
                "Anforderungsmanagement von Individualsoftware",
                "Jira- und Confluence-Admin"
            ],
            tools: [
                "Apache",
                "MySQL Workbench",
                "Netbeans",
                "Jira",
                "Confluence",
                "Baasbox",
                "Cordova"
            ],
            programmiersprachen: [
                {
                    sprache:"Java"
                }, {
                    sprache:"JavaScript",
                    frameworks: [
                        "Framework7"
                        ]
                },
                {
                    sprache: "Shell"
                }
            ]
        }, {
            start: "05.2015",
            ende: "06.2016",
            firma: "PASS Consulting Group",
            position: "Junior Solution Consultant",
            beschreibung: "Wartung und Weiterentwicklung eines komplexen Travel Management Systems.",
            liste: [
                "Entwicklung im Back- und Frontend in Java EE, Javascript und HTML",
                "Mitgestaltung der Architektur",
                "Leitung eines Teams aus drei Entwicklern",
                "Technische Leitung eines Kundenprojekts (Beratung, Definition, Umsetzung)"
            ],
            tools: [
                "Tomcat",
                "Microsoft SQL Management Studio",
                "Altova XML Spy",
                "WinSCP",
                "PuTTY",
                "SVN",
                "Eclipse",
                "Jira"
            ],
            programmiersprachen: [
                {
                    sprache:"Java"
                }, {
                    sprache:"JavaScript",
                    frameworks: [
                        "jQuery"
                        ]
                },
                {
                    sprache: "Shell"
                },
                {
                    sprache: "SQL"
                }
            ]
        }, {
            start: "09.2014",
            ende: "05.2015",
            position: "Freiberuflicher Entwickler",
            beschreibung: "Beratung und Entwicklung von speziellen Anwendungen für Unternehmen.",
            liste: [
                "Anforderungsmanagement und Entwicklung von spezieller Software (Java, Android)",
                "Umsetzung kundenspezifischer Designs (Swing)",
                "Angebotsvergleiche und -bewertungen für Soft- und Hardwareanschaffungen"
            ],
            tools: [
                "IntelliJ IDEA",
                "SVN"
            ],
            programmiersprachen: [
                {
                    sprache:"Java"
                }
            ]
        }, {
            start: "09.2012",
            ende: "02.2015",
            firma: "Garmin Würzburg GmbH",
            position: "Werkstudent/ Masterand",
            beschreibung: "Entwicklung von Methoden zur Korrektur und Vereinfachung der geometrischen Komplexität von Gebäudegrundrissen unter Berücksichtigung großer Datenmengen und kurzer Laufzeiten. Aushilfe in der Testabteilung, hauptsächlich SQA-Core.",
            liste: [
                "Softwareentwicklung in Java",
                "Softwaretests anhand von Testfall- und Anforderungskatalogen",
                "Testautomatisierung in Lua",
                "Vertretung der Teamleitung: Verteilen, Vor- und Nachbereiten von Testläufen"
            ],
            tools: [
                "HP Quality Center",
                "Polarion",
                "Jira",
                "Confluence",
                "Git",
                "SVN",
                "Eclipse",
                "Visual Studio 2012"
            ],
            programmiersprachen: [
                {
                    sprache:"Java"
                },
                {
                    sprache:"Lua"
                },
                {
                    sprache:"SQL"
                }
            ]
        }, {
            start: "09.2011",
            ende: "08.2012",
            firma: "Julius-Maximilians-Universität, Fakultät für Informatik VIII",
            position: "Übungsleiter/ Studentische Hilfskraft",
            beschreibung: "Unter Prof. Dr.-Ing. Kayal.",
            liste: [
                'Leitung und Betreuung der Übungen von "Luft- und Raumfahrttechnik 1" und "Luft- und Raumfahrtbetrieb" mit Java und Matlab',
                "Unterstützung der Lehre von Luft- und Raumfahrtdynamik"
            ],
            tools: [
                "Eclipse",
                "Excel"
            ],
            programmiersprachen: [
                {
                    sprache:"MATLAB"
                },
                {
                    sprache:"Java"
                }
            ]
        }]
    }, {
        titel: "Ausbildung",
        icon: "graduation-cap",
        abschnitte: [{
            start: "10.2012",
            ende: "08.2015",
            firma: "Julius-Maximilians-Universität, Würzburg",
            position: "M. Sc. Informatik",
            beschreibung: 'Masterthesis: <a href="https://vdwals.de/master" class="link external" download target="_blank">Korrektur und Vereinfachung von großen Mengen von Gebäudegrundrissen</a>',
            liste: [
                "Eingebettete Systeme, Algorithmen und Theorie",
                "Projektmanagement",
                'Teilnahme am Projekt <a href="http://www8.informatik.uni-wuerzburg.de/mitarbeiter/kayal0/studentische_aktivitaeten/moon_base_2030/" class="link external">Moonbase 2030</a>'
            ],
            tools: [
                "MikText",
                "TexStudio",
                "R-Studio",
                "Visual Studio",
                "Excel",
                "SVN"
            ],
            programmiersprachen: [
                {
                    sprache:"MATLAB"
                },
                {
                    sprache:"Java"
                },
                {
                    sprache: "Latex"
                },
                {
                    sprache: "R"
                },
                {
                    sprache: "C"
                },
                {
                    sprache: "C++"
                }
            ],
            note: "2,0"
        }, {
            start: "09.2011",
            ende: "08.2014",
            firma: "Julius-Maximilians-Universität, Würzburg",
            position: "B. A. Philosophie und Religion (HF), Physik (NF)",
            beschreibung: 'Schwerpunkte des Studiums waren die modernen Philosophie, altägyptische und altgriechische Religion sowie die Astrophysik.',
            tools: [
                "MikText",
                "Power Point",
                "TexStudio"
            ],
            programmiersprachen: [
                {
                    sprache: "Latex"
                },
                {
                    sprache: "Batch"
                }
            ]
        }, {
            start: "10.2009",
            ende: "08.2012",
            firma: "Julius-Maximilians-Universität, Würzburg",
            position: "B. Sc. Luft- und Raumfahrtinformatik",
            beschreibung: 'Bachelorthesis: <a href="https://vdwals.de/bachelor" class="link external" download target="_blank">Entwurf und Implementierung einer modularen graphischen Benutzeroberfläche für die Darstellung von Echtzeitsatellitentelemetrie</a>',
            liste: [
                "Luft- und Raumfahrttechnik sowie Projektplanung",
                "Eingebettete Systeme",
                'Bahn- und Lagenregelung und -prädiktion von Raumflugkörpern'
            ],
            tools: [
                "MikText",
                "Lab VIEW",
                "Eclipse",
                "Excel",
                "SVN"
            ],
            programmiersprachen: [
                {
                    sprache: "Java"
                },
                {
                    sprache: "Latex"
                }
            ],
            note: "2,2"
        }, {
            start: "09.2008",
            ende: "09.2009",
            firma: "Julius-Maximilians-Universität, Würzburg",
            position: "B. Sc. Physik",
            beschreibung: 'Studienfachwechsel'
        }, {
            exlcudeTimeline: true,
            start: "1998",
            ende: "2007",
            firma: "Missionsgymnasium St. Antonius, Bardel",
            position: "Abitur",
            note: '2,3'
        }]
    }, {
        titel: "Zertifikate",
        excludeTimeline: true,
        icon: "leanpub",
        abschnitte: [{
            zeit: "11.2018",
            firma: "University of Geneva, Coursera",
            position: "Portfolio and Risk Management"
        }, {
            zeit: "11.2018",
            firma: "University of Geneva, Coursera",
            position: "Understanding Financial Markets"
        }, {
            zeit: "11.2018",
            firma: "University of Geneva, Coursera",
            position: "Meeting Investors' Goals"
        }, {
            zeit: "10.2018",
            firma: "Stanford University, Coursera",
            position: "Machine Learning",
            tools: [
                "Mathematica"
            ]
        }, {
            zeit: "2014",
            firma: "Julius-Maximilians-Universität, Würzburg",
            position: "Basiszertifikat im Projektmanagement",
            tools: [
                "MS Project"
            ]
        }]
    }],
    vita: {
        vorname: "Dennis",
        nachname: "van der Wals",
        geburtstag: "08.11.1987",
        familienstand: "verheiratet",
        geburtsort: "48599 Gronau (Westf.)",
        adresse: {
            strasse: "Gebeschusstr.",
            nummer: "33",
            plz: "65929",
            stadt: "Frankfurt am Main"
        },
        kontakte: [{
            text: "0177 / 858 9207",
            beschreibung: "Anrufen",
            link: "tel:00491778589207",
            icon: "phone_fill"
        },{
            text: "live:dvdw_1",
            beschreibung: "Anrufen",
            link: "skype:live:dvdw_1",
            fa_icon: "skype"
        },{
            text: "dennis@vdwals.de",
            beschreibung: "Email",
            link: "mailto:dennis@vdwals.de?subject=Lebenslauf",
            icon: "email_fill"
        },{
            text: "Linkedin",
            beschreibung: "Öffnen",
            link: "https://www.linkedin.com/in/dennis-van-der-wals-265b22166/",
            icon: "logo_linkedin"
        },{
            text: "vdwals",
            beschreibung: "Öffnen",
            link: "https://bitbucket.org/vdwals/",
            img: "./data/images/bitbucket-icon-20.jpg"
        }]
    },
    faehigkeiten: [{
        titel: "Sprachen",
        liste: [
            {
                titel: "Deutsch",
                level: 5,
                beschreibung: "Muttersprache"
            },
            {
                titel: "Englisch",
                level: 3.5,
                beschreibung: "B2"
            },
            {
                titel: "Schwedisch",
                level: 1.5,
                beschreibung: "A2"
            },
            {
                titel: "Niederländisch",
                level: 1,
                beschreibung: "A"
            }
            ]
    },{
        titel: "Programmiersprachen",
        lebenslauf: "programmiersprachen",
        liste: [
            {
                titel: "Java",
                level: 4
            },
            {
                titel: "JavaScript",
                level: 3
            },
            {
                titel: "SQL",
                level: 2.5
            },
            {
                titel: "C",
                level: 0.75
            },
            {
                titel: "C++",
                level: 0.5
            }
            ]
    },{
        titel: "Anwendungen",
        lebenslauf: "tools",
        liste: [
            {
                titel: "Eclipse",
                level: 4
            },
            {
                titel: "IntelliJ IDEA",
                level: 2
            },
            {
                titel: "Visual Studio Code",
                level: 3
            },
            {
                titel: "DBeaver",
                level: 3.5
            }
            ]
    }]
};