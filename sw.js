self.importScripts('data/cv.js');

// Files to cache
var cacheName = 'cv1.0';
var appShellFiles = [
  '/css/framework7.min.css',
  '/css/framework7-icons.css',
  '/css/timesheet.min.css',
  '/css/timesheet-white.min.css',
  '/fonts/Framework7Icons-Regular.eot',
  '/fonts/Framework7Icons-Regular.ttf',
  '/fonts/Framework7Icons-Regular.woff',
  '/fonts/Framework7Icons-Regular.woff2',
  '/data/images/bitbucket-icon-20.jpg',
  '/data/images/profil-1.jpg',
  '/data/images/profil-2.jpg',
  '/data/images/profil-3.jpg',
  '/data/images/qr.png'/*,
  '/',
  '/index.html',
  '/css/app.css',
  '/data/cv.js',
  '/js/app.js',
  '/js/lib/framework7.js',
  '/pages/timeline.html'*/
];

var contentToCache = appShellFiles;

// Activating Service Worker
self.addEventListener('activate', function(event) {
    console.log('[Service Worker] Activating', event);
    event.waitUntil(
        caches.keys()
            .then(function (keyList) {
                return Promise.all(keyList.map(function (key) {
                    if (key !== cacheName) {
                        console.log("[Service Worker] Removing old");
                        return caches.delete(key);
                    }
                }));
            })
    );
    return self.clients.claim();
});

// Installing Service Worker
self.addEventListener('install', function(e) {
  console.log('[Service Worker] Install');
  e.waitUntil(
    caches.open(cacheName).then(function(cache) {
      console.log('[Service Worker] Caching all: app shell and content');
      return cache.addAll(contentToCache);
    })
  );
});

// Fetching content using Service Worker
self.addEventListener('fetch', function(e) {
  e.respondWith(
      fetch(e.request)
        .then(function (res) {
            return caches.open(cacheName)
                .then(function(cache) {
                    console.log('[Service Worker] Caching resource: '+ e.request.url);
                    cache.put(e.request.url, res.clone());
                    return res;
                });
        })
        .catch(function(err) {
            return caches.match(e.request);
        })
  );
});