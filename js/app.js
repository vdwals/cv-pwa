/*jshint moz: true, esnext: true */

Template7.registerHelper('log', function (arr, options) {
  // First we need to check is the passed arr argument is function
  console.log(arr);
});

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

/* -- Prepare Data -- */
const expertLevel= [
    "Begrenzte Erfahrung",
    "Praktische Erfahrung",
    "Fortgeschritten",
    "Senior",
    "Experte"
    ];

function prepareData() {
    data.pic = Math.floor((Math.random() * 3) + 1);
    
    for (const faehigkeit of data.faehigkeiten) {
        if (faehigkeit.lebenslauf) {
            // Dursuche alle Abschnitte nach passenden Fähigkeiten aus der aktuellen Rubrik
            data.lebenslauf.map((part) => {
                return part.abschnitte.filter((abschnitt) => {
                    return abschnitt[faehigkeit.lebenslauf];
                }).map((abschnitt) => {
                    return {start: abschnitt.start, ende: abschnitt.ende, f: abschnitt[faehigkeit.lebenslauf]};
                });
            }).forEach((part) => {
                part.forEach((abschnitt) => {
                    
                    // Verarbeite Abschnitt
                    if (abschnitt.start) {
                        // Berechne, wie lange die Fähigkeit verwendet wurde.
                        const startDate = new Date(abschnitt.start.replace('.', '/01/'));
                        let endDate = new Date();
                        
                        if (abschnitt.ende) {
                            endDate = new Date(abschnitt.ende.replace('.', '/01/'));
                        }
                        
                        const incDauer = endDate - startDate;
                        
                        // Reduziere Fähigkeitenliste
                        const f = abschnitt.f.map((a) => {
                            return (typeof a === 'object')?a:{sprache: a};
                        });
                        
                        // Füge alle Fähigkeiten oder dauern hinzu
                        f.forEach((fs) => {
                            let exists = false;
                            
                            // Suche existierenden Eintrag.
                            let existingItem = faehigkeit.liste.filter((item) => {return item.titel.toLowerCase() == fs.sprache.toLowerCase();})[0];
                            
                            if (existingItem) {
                                // Lege dauer an.
                                if (!existingItem.dauer) {
                                    existingItem.dauer = 0;
                                }
                                
                                existingItem.dauer += incDauer;
                            } else {
                                // Lege Eintrag an
                                existingItem = {
                                    titel: fs.sprache,
                                    dauer: incDauer
                                };
                                faehigkeit.liste.push(existingItem);
                            }
                            
                            if (!existingItem.start || existingItem.start > startDate) {
                                existingItem.start = startDate;
                            }
                            if (!existingItem.ende || existingItem.ende < endDate) {
                                existingItem.ende = endDate;
                            }
                            
                            if (fs.frameworks) {
                                // Lege Liste an
                                if (!existingItem.frameworks) {
                                    existingItem.frameworks = [];
                                }
                                
                                fs.frameworks.forEach((framework) => {
                                    let existing = existingItem.frameworks.filter((test) => {
                                        return test.titel.toLowerCase == framework;
                                    })[0];
                                    
                                    if (!existing) {
                                        existing = {
                                            titel: framework,
                                            dauer: incDauer
                                        };
                                        existingItem.frameworks.push(existing);
                                    } else {
                                        if (!existing.dauer) {
                                            existing.dauer = 0;
                                        }
                                        existing.dauer += incDauer;
                                    }
                                    
                            
                                    if (!existing.start || existing.start > startDate) {
                                        existing.start = startDate;
                                    }
                                    if (!existing.ende || existing.ende < endDate) {
                                        existing.ende = endDate;
                                    }
                                });
                            }
                            
                        });
                    }
                });
            });
        }
    }
        
    const listen = [];
    let max, min;
    data.faehigkeiten.filter((faehigkeit) => {return faehigkeit.liste}).map((faehigkeit) => {return faehigkeit.liste;}).forEach((liste) => {
        liste.forEach((l) => {
            listen.push(l);
            
            l.dauer = l.ende - l.start;
            
            if (!max || max < l.dauer) {
                max = l.dauer;
            }
            if (!min || min > l.dauer) {
                min = l.dauer;
            }
        });
    });
    
    listen.forEach((item) => {
        if (!item.level && item.dauer) {
            item.level = item.dauer * expertLevel.length / max;
        }
        if (item.level && !item.beschreibung) {
            item.beschreibung = expertLevel[Math.floor(item.level)];
        }
    });
    
    console.log(data);
}
prepareData();

/* -- */

// Global Consts
const $$ = Dom7;

const app = new Framework7({
  // App root element
  root: "#app",
  // App Name
  name: "Dennis' CV",
  // App id
  id: "de.etern.it.cv",
  routes: [
    {
        path: "/",
        url: "index.html",
        tabs: [
          {
            path: "/",
            id: "home-tab",
            templateUrl: "./pages/beschreibung.html"
          },
          {
            path: "/cv/",
            id: "cv-tab",
            templateUrl: "./pages/timeline.html",
            options: {
                context: data
            }
          },
          {
            path: "/faehigkeiten/",
            id: "faehigkeiten-tab",
            templateUrl: "./pages/faehigkeiten.html"
          }
        ],
        options: {
            context: data
        }
      }
  ],
  on: {
      tabInit: function(tabEl, route) {
            console.log(route);
            $el = $$(tabEl);
            
            switch(route.id) {
                case "home-tab":
                    data.pic = Math.floor((Math.random() * 3) + 1);
                    break;
                case "faehigkeiten-tab":
                    const listen = [];
                    data.faehigkeiten.filter((faehigkeit) => {return faehigkeit.liste}).map((faehigkeit) => {return faehigkeit.liste;}).forEach((liste) => {
                        liste.forEach((l) => {
                            listen.push(l);
                        });
                    });
                    
                    for (let i = 1; i <= 100; i++) {
                        setTimeout(function() {
                            listen.forEach((item) => {
                                if (item.level) {
                                    app.progressbar.set('#progress-' + item.titel.replaceAll(' ',''), Math.min(i,(item.level / expertLevel.length) * 100));
                                }
                            });
                        }, i * 30);
                    }
                    break;
                case "cv-tab":
                    if ($el.find('#timesheet') && route.options && route.options.context && route.options.context.lebenslauf) {
                        initTimeline($el, route.options.context.lebenslauf);
                    }
                    break;
            }
        }
  }
});

app.router.navigate('/');

function initTimeline($el, lebenslauf) {
    const timesArr = [];
    let minTime, maxTime;
    
    const date = new Date();
    const now = (date.getMonth() + 1) + '/' + date.getFullYear();
    
    maxTime = date;
    
    const insertIntoArray = function(row) {
        const dRow = Date.parse(row[0].replace("/","/01/"));
        let index = 0;
        
        if (!minTime || dRow < minTime) {
            minTime = dRow;
        }
        if (!maxTime || dRow > maxTime) {
            maxTime = dRow;
        }
        
        for(index; index < timesArr.length; index++) {
            const oRow = Date.parse(timesArr[index][0].replace("/","/01/"));
            if (oRow > dRow) {
                break;
            }
        }
        
        timesArr.splice(index, 0, row);
    };
    
    for (const part of lebenslauf) {
        if (!part.excludeTimeline) {
            for (const abschnitt of part.abschnitte) {
                
                if (!abschnitt.exlcudeTimeline) {
                    const row = [];
                    row.push(abschnitt.start.replace(".","/"));
                    
                    if (abschnitt.ende) {
                        row.push(abschnitt.ende.replace(".","/"));
                    } else {
                        row.push(now);
                    }
                    
                    row.push(abschnitt.position);
                    row.push(part.titel);
                    
                    insertIntoArray(row);
                }
            }
        }
    }
    
    
    
    new Timesheet('timesheet', new Date(minTime).getFullYear(), (new Date(maxTime).getFullYear()) + 1, timesArr);
    
    $el.find('.scale').addClass('timeline-overwidth');
    $el.find('.data').addClass('timeline-overwidth');
}
